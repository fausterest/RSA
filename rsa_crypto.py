import random

PRIME_CONF = 8


class PublicKey:
    length: int

    n: int
    e: int

    def __init__(self, length: int, n: int, e: int):
        self.length = length
        self.n = n
        self.e = e


class PrivateKey:
    length: int

    n: int
    d: int

    def __init__(self, length: int, n: int, d: int):
        self.length = length
        self.n = n
        self.d = d


def setbit(number, index, value):
    mask = 1 << index
    number &= ~mask
    if value:
        number |= mask
    return number


def miller_rabin_test(a, s, d, n):
    atop = pow(a, d, n)
    if atop == 1:
        return True
    for i in range(s - 1):
        if atop == n - 1:
            return True
        atop = pow(atop, atop, n)
    return atop == n - 1


def miller_rabin(n, confidence):
    d = n - 1
    s = 0

    while d % 2 == 0:
        d >>= 1
        s += 1

    for i in range(confidence):
        a = 0
        while a == 0:
            a = random.randrange(n)
        if not miller_rabin_test(a, s, d, n):
            return False
    return True


def get_prime_number(length: int):
    def init_number():
        p = random.getrandbits(length)
        p = setbit(p, length - 1, 1)
        p = setbit(p, 0, 1)
        return p

    p = init_number()
    while not miller_rabin(p, PRIME_CONF):
        p = init_number()

    return p


def egcd(a, b):
    if a == 0:
        return b, 0, 1
    else:
        g, y, x = egcd(b % a, a)
        return g, x - (b // a) * y, y


def modinv(a, m):
    g, x, y = egcd(a, m)
    if g != 1:
        raise Exception('Modular inverse does not exist')
    else:
        return x % m


def generate_keys(length: int):
    q = get_prime_number(length // 2)
    print('q: ', q)

    p = get_prime_number(length // 2)
    print('p: ', p)

    n = p * q
    e = get_prime_number(length // 4)
    print('e: ', e)

    public_key = PublicKey(length, n, e)

    phi = (p - 1) * (q - 1)
    d = modinv(e, phi)
    private_key = PrivateKey(length, n, d)

    return public_key, private_key


def string_to_blocks(string: str, n: int):
    char_list = [ord(chars) for chars in string]

    if len(char_list) % n != 0:
        for i in range(0, n - len(char_list) % n):
            char_list.append(0)

    blocks = []
    for i in range(0, len(char_list), n):
        block = 0
        for j in range(0, n):
            block += char_list[i + j] << (8 * (n - j - 1))
        blocks.append(block)
    return blocks


def blocks_to_string(blocks: list, n: int):
    char_list = []
    for block in blocks:
        inner = []
        for i in range(0, n):
            inner.append(block % 256)
            block >>= 8

        inner.reverse()
        char_list.extend(inner)
    return ''.join(map(chr, char_list))


def encrypt(string: str, public_key: PublicKey):
    result = []
    for i in string_to_blocks(string, public_key.length // 8):
        result.append(pow(i, public_key.e, public_key.n))

    return result


def decrypt(cipher: list, private_key: PrivateKey):
    blocks = []
    for i in cipher:
        blocks.append((int(pow(i, private_key.d, private_key.n))))

    return blocks_to_string(blocks, private_key.length // 8)


public, private = generate_keys(2048)

print('public: ', hex(public.n))
print('private: ', hex(private.d))

print("public length", public.n.bit_length())
print("private length", private.n.bit_length())

source = "Hello world!"
print("Source: ", source)

encrypted = encrypt(source, public)
# print([hex(i) for i in encrypted])
print("Encrypted: ", encrypted)

decrypted = decrypt(encrypted, private)
print("Decrypted: ", decrypted)
